package it.unibo.oop.lab.nesting2;
import java.util.*;

public class OneListAcceptable<T> implements Acceptable<T> {
	
	private final List<T> list;
	private Iterator<T> iterator;
	
	public OneListAcceptable(List<T> list) {
		this.list = list;
		this.iterator = this.list.iterator();
	}

	public Acceptor<T> acceptor() {
		return new Acceptor<T>() {
			public void accept(T newElement) throws ElementNotAcceptedException {
				if((!iterator.hasNext()) || (iterator.next()!=newElement)) {
					throw new ElementNotAcceptedException(newElement);
				}
			}
			public void end() throws EndNotAcceptedException {
				if(iterator.hasNext()) {
					throw new EndNotAcceptedException();
				}
			}
		};
		
	}
}
